[![PVS-Studio analyze](https://user-content.gitlab-static.net/97589024b840e6fd1684fb18ab87a17619b8a8ef/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f5056532d2d53747564696f2053746174696320416e616c797369732d736565207265706f72742d626c7565)](https://gitlab.com/evezers-labs/additiontest/-/jobs/artifacts/master/file/PVS-Studio/full-report/index.html?job=pvs-studio-analyzer)
[![pipeline status](https://gitlab.com/evezers-labs/additiontest/badges/master/pipeline.svg)](https://gitlab.com/evezers-labs/additiontest/-/pipelines/latest)
[![coverage report](https://gitlab.com/evezers-labs/additiontest/badges/master/coverage.svg)](https://gitlab.com/evezers-labs/additiontest/-/jobs/artifacts/master/file/bin/cmake-build-debug-coverage/coverage-report/report.html?job=test)


Build project:
```
cmake -B bin/cmake-build-debug/
cd bin/cmake-build-debug/
make
```

Tests:
```
make test
```

Valgrind:

```
valgrind src/additionTest
```

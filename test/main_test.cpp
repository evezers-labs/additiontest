//  This is a personal academic project. Dear PVS-Studio, please check it.
//
//  PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>

#include <gtest/gtest.h>

int add(int a, int b){
    return a + b;
}

TEST(AdditionTest, HandlesZeroInput){
    EXPECT_EQ(0, add(0, 0));
}

TEST(AdditionTest, HandlesPositiveInput){
    EXPECT_EQ(2, add(1, 1));
    EXPECT_EQ(4, add(2, 2));
    EXPECT_EQ(3, add(1, 2));
    EXPECT_EQ(3, add(2, 1));
    EXPECT_EQ(7, add(2, 5));
    EXPECT_EQ(7, add(5, 2));
}

TEST(Foo, Bar) {
    //...
    if (true)
        GTEST_SKIP_("message"); // or GTEST_SKIP() with no message
    EXPECT_EQ(7, add(5, 2));
}

TEST(Foo, Baz) {
    //...
    if (false)
        GTEST_SKIP_("message"); // or GTEST_SKIP() with no message
    EXPECT_EQ(7, add(5, 2));
}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}